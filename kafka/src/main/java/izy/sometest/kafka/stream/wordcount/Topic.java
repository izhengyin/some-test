package izy.sometest.kafka.stream.wordcount;

/**
 * Created by zhengyin on 2017/8/21 下午3:13.
 * Email  zhengyinit@outlook.com
 */
public interface Topic {
    public static final String NAME = "stream-wordcount";
}
