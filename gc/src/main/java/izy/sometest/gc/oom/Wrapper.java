package izy.sometest.gc.oom;

import izy.sometest.gc.Memory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 *
 * -XX:+UnlockCommercialFeatures -Xmx128M -verbose:gc -XX:+PrintGCDetails
 * @author zhengyin
 * Created on 2021/9/17
 */
public class Wrapper {
    public static void main(String args[]) throws Exception
    {
        Map<Integer,String> map = new HashMap();
        Random r = new Random();
        while (true) {
            //10000 ~ 100000 观察GC的耗时
            for (int i=0;i<10000;i++){
                map.put(r.nextInt(2200000), "randomValue");
            }
            TimeUnit.MILLISECONDS.sleep(100);
            Memory.printUseMemory();
        }
    }
}
