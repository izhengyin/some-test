package izy.sometest.gc;

/**
 * @author zhengyin
 * Created on 2021/9/17
 */
public class Memory {
    private final static Runtime runtime = Runtime.getRuntime();
    public static void printUseMemory(){
        System.out.println(((runtime.totalMemory() - runtime.freeMemory()) / 1024) + "kb");
    }
}
