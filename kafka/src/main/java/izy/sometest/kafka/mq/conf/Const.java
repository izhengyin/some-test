package izy.sometest.kafka.mq.conf;

/**
 * Created by zhengyin on 2017/8/21 下午3:06.
 * Email  zhengyinit@outlook.com
 */
public interface Const {
    public static final String BORKER_0 = "localhost:9092";
    public static final String BORKER_1 = "localhost:9093";
    public static final String BORKER_2 = "localhost:9094";
    public static final String BORKER_LIST = BORKER_0;
    public static final String KEY_SERIALIZER_CLASS_CONFIG = "org.apache.kafka.common.serialization.StringSerializer";
    public static final String VALUE_SERIALIZER_CLASS_CONFIG = "org.apache.kafka.common.serialization.StringSerializer";
    public static final String KEY_DESERIALIZER_CLASS_CONFIG = "org.apache.kafka.common.serialization.StringDeserializer";
    public static final String VALUE_DESERIALIZER_CLASS_CONFIG = "org.apache.kafka.common.serialization.StringDeserializer";
}