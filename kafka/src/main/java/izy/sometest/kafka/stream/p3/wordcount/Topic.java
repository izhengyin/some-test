package izy.sometest.kafka.stream.p3.wordcount;

/**
 * Created by zhengyin on 2017/8/21 下午3:13.
 * Email  zhengyinit@outlook.com
 */
public interface Topic {
    public static final String NAME = "wordcount-p3";
}
