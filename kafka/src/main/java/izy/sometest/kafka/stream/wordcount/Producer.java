package izy.sometest.kafka.stream.wordcount;

import izy.sometest.kafka.mq.conf.Const;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Kafka Producer
 */
public class Producer implements Runnable{
    private static volatile boolean runing = true;
    private final KafkaProducer<String,String> producer;
    private final String topic;
    private final int id;
    public Producer(int id , String topic){
        Properties props = new Properties();
        props.put("bootstrap.servers", Const.BORKER_LIST);
        props.put("client.id", Producer.class.getName()+"."+id);
        props.put("key.serializer", Const.KEY_SERIALIZER_CLASS_CONFIG);
        props.put("value.serializer", Const.VALUE_SERIALIZER_CLASS_CONFIG);
        this.producer = new KafkaProducer<>(props);
        this.topic = topic;
        this.id = id;
    }
    @Override
    public void run() {
        String[] words = new String[]{"上海","成都","北京","杭州","厦门","西安","拉萨","深圳","广州","重庆","三亚"};
        Random random = new Random();
        while (true){
            int i = random.nextInt(words.length);
            String message = words[i];
            long startTime = System.currentTimeMillis();
            ProducerRecord<String,String> record = new ProducerRecord<>(topic,i+"",message);
            Future<RecordMetadata> future = producer.send(record);
            try {
                RecordMetadata metadata = future.get();
                long elapsedTime = System.currentTimeMillis() - startTime;
                if(metadata != null){
                    System.out.println(
                            "Producer["+id+"] Success: message(" +record.key()+" => "+ record.value() + ") sent to partition(" + metadata.partition() +
                                    "), " +
                                    "offset(" + metadata.offset() + ") in " + elapsedTime + " ms");
                }else{
                    System.out.println(
                            "Producer["+id+"] Error: message(" + record.value() + ")   " +
                                    " in " + elapsedTime + " ms  ");
                }
                TimeUnit.MILLISECONDS.sleep(100);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
        new Producer(0, Topic.NAME)
                .run();
    }
}
