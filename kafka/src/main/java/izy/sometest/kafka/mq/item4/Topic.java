package izy.sometest.kafka.mq.item4;

/**
 * Created by zhengyin on 2017/8/21 下午3:13.
 * Email  zhengyinit@outlook.com
 */
public interface Topic {
    public static final String NAME = "test.kafka.topic.item4";
}
