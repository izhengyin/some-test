package izy.sometest.sentinel;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SentinelApplicationTests {
    static boolean boolValue;
    public static void main(String[] args) {
        boolValue = true; // 将这个true替换为2或者3，再看看打印结果
        if (boolValue) System.out.println("Hello, Java!");
        if (boolValue == true) System.out.println("Hello, JVM!");
    }
    @Test
    void contextLoads() {

    }

}
