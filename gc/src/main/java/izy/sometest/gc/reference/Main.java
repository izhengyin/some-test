package izy.sometest.gc.reference;
import org.testng.Assert;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * @author zhengyin
 * Created on 2021/9/17
 */
public class Main {

    private final static Runtime runtime = Runtime.getRuntime();

    public static void main(String[] args) {
        sortReference();
        weakReference();
        weakHashMap();
    }

    /**
     * 堆区满了以后，会进行回收
     */
    static void sortReference(){
        Object obj = new Object();
        SoftReference<Object> ref = new SoftReference<Object>(obj);
        obj = null;
        System.gc();
        Assert.assertNotNull(ref.get());

    }

    /**
     * GC 会进行回收
     */
    static void weakReference(){
        Object obj = new Object();
        WeakReference<Object> ref = new WeakReference<Object>(obj);
        obj = null;
        System.gc();
        Assert.assertNull(ref.get());
    }

    static void weakHashMap(){
        WeakHashMap<Integer,Object> weakHashMap = new WeakHashMap<Integer, Object>();
        IntStream.range(1,Integer.MAX_VALUE)
            .forEach(n -> {
                for (int i=0;i<100000;i++){
                    weakHashMap.put(i,new Object());
                    weakHashMap.remove(i);
                }
                System.out.println("usedMemory "+(usedMemory() / 1024 / 1024) +"m");
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                }catch (InterruptedException e){

                }

            });
    }

    /**
     * 堆中已使用内存
     * @return 堆中已使用内存
     */
    private static long usedMemory() {
        return runtime.totalMemory() - runtime.freeMemory();
    }
}
