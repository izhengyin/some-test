package izy.sometest.kafka.stream.p3.wordcount;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;

import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Kafka Stream Consumer
 */
public class StreamConsumer implements Runnable {

    @Override
    public void run() {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG,StreamConsumer.class.getName());
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        props.put(ConsumerConfig.GROUP_ID_CONFIG,StreamConsumer.class.getSimpleName());
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> source = builder.stream(Topic.NAME);
        //每分钟统计一次，按照value进行分组
        KTable<Windowed<String>, Long> counts = source
                .groupBy((key,value) -> value)
                .windowedBy(TimeWindows.of(Duration.ofMinutes(1)))
                .count();
        // 将windowed转换为String
        counts.toStream()
                .map(((windowed, aLong) -> new KeyValue<>(windowed.toString(),aLong)))
                .peek((k,v) -> System.out.println(Thread.currentThread().getName()+" : "+k +" => "+v))
                .to("streams-wordcount-output", Produced.with(Serdes.String(), Serdes.Long()));
        final KafkaStreams streams = new KafkaStreams(builder.build(), props);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            streams.close();
        }));
        streams.start();
    }

    public static void main(String[] args) throws Exception {
        new StreamConsumer()
                .run();
    }

}
